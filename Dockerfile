FROM golang:latest AS build
RUN go get github.com/vkcom/kittenhouse

FROM debian
COPY --from=build /go/bin/kittenhouse /kittenhouse
ENTRYPOINT [ "/kittenhouse" ]
CMD [ "--help" ]
